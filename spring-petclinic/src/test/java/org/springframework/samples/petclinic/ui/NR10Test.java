package org.springframework.samples.petclinic.ui;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR10Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
	  driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }
  
  @Test
  public void testNR10() throws Exception {
    driver.get("http://localhost:50000/");
    driver.findElement(By.linkText("FIND OWNERS")).click();
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Prueba");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("McPrueba");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Prueba");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Prueba");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("600000000");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pets and Visits'])[1]/preceding::a[1]")).click();
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Pruebinha");
    driver.findElement(By.id("birthDate")).clear();
    driver.findElement(By.id("birthDate")).sendKeys("1990-02-28");
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("20.0");
    driver.findElement(By.id("comments")).click();
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("Pruebas");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
    assertEquals("FIND PETS", driver.findElement(By.linkText("FIND PETS")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find owners'])[1]/following::span[2]")).click();
    assertEquals("Find Pets", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::h2[1]")).getText());
    assertEquals("Name", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find Pets'])[1]/following::label[1]")).getText());
    assertEquals("Find Pet", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::button[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::button[1]")).click();
    assertEquals("Pruebinha", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Carlos Estaban'])[2]/following::td[1]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[2]")).click();
    assertEquals("Prueba McPrueba", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='bird'])[3]/following::td[1]")).getText());
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
