package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTests {
	
	public static Pet pet;	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		pet=new Pet();		
	}

	@Before
	public void setUp() throws Exception {
		pet.setWeight(5);
		pet.setComments("PruebaNR1");
	}
	
	@Test
	public void getWeightTest() {
		assertNotNull(pet.getWeight());
		assertTrue(pet.getWeight()==5);
	}

	@Test
	public void setWeigthTest() {
		float nuevoPeso=7;
		assertTrue(pet.getWeight()==5);
		pet.setWeight(nuevoPeso);
		assertFalse(pet.getWeight()==5);
		assertTrue(pet.getWeight()==nuevoPeso);
	}
	
	@Test
	public void getCommentsTest() {
		assertNotNull(pet.getComments());
		assertTrue(pet.getComments()=="PruebaNR1");
	}

	@Test
	public void setCommentsTest() {
		String nuevaCadena="PruebaError";
		assertTrue(pet.getComments()=="PruebaNR1");
		pet.setComments(nuevaCadena);
		assertFalse(pet.getComments()=="PruebaNR1");
		assertTrue(pet.getComments()==nuevaCadena);
	}


}
