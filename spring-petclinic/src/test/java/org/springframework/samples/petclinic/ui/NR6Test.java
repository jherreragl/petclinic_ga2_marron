package org.springframework.samples.petclinic.ui;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class NR6Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
    driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testNR6() throws Exception {
    driver.get("http://localhost:50000/");
    driver.findElement(By.linkText("FIND OWNERS")).click();
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Yu");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Narukami");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Inaba St.");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Tokyo");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("600000000");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pets and Visits'])[1]/preceding::a[1]")).click();
    driver.findElement(By.id("name")).click();
    driver.findElement(By.id("name")).clear();
    driver.findElement(By.id("name")).sendKeys("Kuma");
    driver.findElement(By.id("birthDate")).clear();
    driver.findElement(By.id("birthDate")).sendKeys("2004-12-02");
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("12.5");
    driver.findElement(By.id("comments")).clear();
    driver.findElement(By.id("comments")).sendKeys("Test pet");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::a[2]")).click();
    driver.findElement(By.id("date")).click();
    driver.findElement(By.id("date")).clear();
    driver.findElement(By.id("date")).sendKeys("2011-01-20");
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Test visit");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).click();
    assertEquals("Edit", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Description'])[1]/following::th[1]")).getText());
    assertEquals("Edit", driver.findElement(By.linkText("Edit")).getText());
    driver.findElement(By.linkText("Edit")).click();
    assertEquals("2011-01-20", driver.findElement(By.id("date")).getAttribute("value"));
    assertEquals("Test visit", driver.findElement(By.id("description")).getAttribute("value"));
    assertEquals("Update Visit", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).getText());
    driver.findElement(By.id("date")).click();
    driver.findElement(By.id("date")).clear();
    driver.findElement(By.id("date")).sendKeys("2011-01-22");
    driver.findElement(By.id("description")).click();
    driver.findElement(By.id("description")).clear();
    driver.findElement(By.id("description")).sendKeys("Test visit edited");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Vet'])[1]/following::button[1]")).click();
    assertEquals("2011-01-22", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::td[1]")).getText());
    assertEquals("Test visit edited", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::td[2]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
