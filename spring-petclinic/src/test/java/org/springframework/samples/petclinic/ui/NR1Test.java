package org.springframework.samples.petclinic.ui;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR1Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
	driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testWeightComments() throws Exception {
	  driver.get("http://localhost:50000/");
	    driver.findElement(By.linkText("FIND OWNERS")).click();
	    driver.findElement(By.linkText("Add Owner")).click();
	    driver.findElement(By.id("firstName")).click();
	    driver.findElement(By.id("firstName")).clear();
	    driver.findElement(By.id("firstName")).sendKeys("Pepito");
	    driver.findElement(By.id("lastName")).click();
	    driver.findElement(By.id("lastName")).clear();
	    driver.findElement(By.id("lastName")).sendKeys("Tacatum");
	    driver.findElement(By.id("address")).click();
	    driver.findElement(By.id("address")).clear();
	    driver.findElement(By.id("address")).sendKeys("C/Falsa 123");
	    driver.findElement(By.id("city")).click();
	    driver.findElement(By.id("city")).clear();
	    driver.findElement(By.id("city")).sendKeys("NoExiste");
	    driver.findElement(By.id("telephone")).click();
	    driver.findElement(By.id("telephone")).clear();
	    driver.findElement(By.id("telephone")).sendKeys("999999999");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Pets and Visits'])[1]/preceding::a[1]")).click();
	    driver.findElement(By.id("name")).click();
	    driver.findElement(By.id("name")).clear();
	    driver.findElement(By.id("name")).sendKeys("PruebaPerro");
	    driver.findElement(By.id("birthDate")).click();
	    driver.findElement(By.id("birthDate")).clear();
	    driver.findElement(By.id("birthDate")).sendKeys("2018-02-02");
	    driver.findElement(By.id("type")).click();
	    new Select(driver.findElement(By.id("type"))).selectByVisibleText("dog");
	    driver.findElement(By.id("type")).click();
	    driver.findElement(By.id("weight")).click();
	    assertEquals("0.0", driver.findElement(By.id("weight")).getAttribute("value"));
	    driver.findElement(By.id("comments")).click();
	    driver.findElement(By.id("comments")).clear();
	    driver.findElement(By.id("comments")).sendKeys("Test");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::a[1]")).click();
	    driver.findElement(By.id("weight")).click();
	    assertEquals("0.0", driver.findElement(By.id("weight")).getAttribute("value"));
	    driver.findElement(By.id("comments")).click();
	    assertEquals("Test", driver.findElement(By.id("comments")).getAttribute("value"));
	    driver.findElement(By.id("weight")).click();
	    driver.findElement(By.id("weight")).click();
	    driver.findElement(By.id("weight")).clear();
	    driver.findElement(By.id("weight")).sendKeys("50.0");
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::button[1]")).click();
	    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::a[1]")).click();
	    driver.findElement(By.id("weight")).click();
	    assertEquals("50.0", driver.findElement(By.id("weight")).getAttribute("value"));
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
