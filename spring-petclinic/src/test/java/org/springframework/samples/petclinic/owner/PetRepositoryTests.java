package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {
	
	@Autowired
	private PetRepository pets;
	
	private Pet pet;
	
	private Owner owner;
	
	@Autowired
	private OwnerRepository owners;
	
	private PetType pettype;
	
	@Before
	public void init() {
		//Collection <Vet> cVets = 
		
		if (this.pet==null) {
			pet = new Pet ();
			pet.setName("Kuma");
			pet.setBirthDate(LocalDate.now());
			pet.setComments("Comentarios de prueba");
			pet.setId(1);
			owner=owners.findById(1);
			if(owner==null) {
				owner = new Owner();
				owner.setAddress("Test");
				owner.setCity("Test");
				owner.setFirstName("Test");
				owner.setId(1);
				owner.setLastName("Test");
				owner.setTelephone("7357");
				owners.save(owner);
			}
			pet.setOwner(owner);
			if(pets.findPetTypes().isEmpty()) {
				pettype = new PetType();
				pettype.setId(1);
				pettype.setName("Test");
			}
			else {
				pettype = pets.findPetTypes().get(0);
			}
			pet.setType(pettype);
			pet.setWeight((float) 50.0);
			this.pets.save(pet);
		}
	}
	
	@Test
	public void testFindByName() {
		Collection<Pet> petsFindByName = this.pets.findPetsByName("Kuma");
		
		assertNotNull(petsFindByName);
		assertFalse(petsFindByName.isEmpty());
		
		if(petsFindByName.size()==1) { //Caso en el que solo está el que hemos añadido
			Pet testPet = petsFindByName.iterator().next();
			assertEquals(this.pet.getName(), testPet.getName());
			assertEquals(this.pet.getComments(), testPet.getComments());
			assertEquals(this.pet.getBirthDate(), testPet.getBirthDate());
			assertEquals(this.pet.getWeight(), testPet.getWeight(), 0);
			Owner owner = testPet.getOwner();
			assertEquals(this.owner.getAddress(), owner.getAddress());
			assertEquals(this.owner.getCity(), owner.getCity());
			assertEquals(this.owner.getFirstName(), owner.getFirstName());
			assertEquals(this.owner.getLastName(), owner.getLastName());
			PetType pettype = testPet.getType();
			assertEquals(this.pettype.getId(), pettype.getId());
			assertEquals(this.pettype.getName(), pettype.getName());
		}
		
	}
	
	@Test
	public void testDeletePet() {
		Integer sizeOriginal = this.pets.findPetsByName("Kuma").size();
		this.pets.deletePetInfo(pet.getId());
		this.pets.delete(this.pet);
		assertEquals(this.pets.findPetsByName("Kuma").size(), sizeOriginal-1);
	}
	
	
	@After
	public void finish() {
		pets.delete(this.pet);
		this.pet=null;
		this.owner=null;
		this.pettype=null;
	}
	

}