package org.springframework.samples.petclinic.vet;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

/**
 * Test class for the {@link VetController}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(VetController.class)
public class VetControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VetRepository vets;

    private static int TEST_VET_ID=1;
    
    private Vet james;
    
    @Before
    public void setup() {
        james = new Vet();
        james.setFirstName("James");
        james.setLastName("Carter");
        james.setId(1);
        Vet helen = new Vet();
        helen.setFirstName("Helen");
        helen.setLastName("Leary");
        helen.setId(2);
        Specialty radiology = new Specialty();
        radiology.setId(1);
        radiology.setName("radiology");
        helen.addSpecialty(radiology);
        given(this.vets.findAll()).willReturn(Lists.newArrayList(james, helen));
        given(this.vets.findById(TEST_VET_ID)).willReturn(james);
    }

    @Test
    public void testShowVetListHtml() throws Exception {
        mockMvc.perform(get("/vets.html"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("vets"))
            .andExpect(view().name("vets/vetList"));
    }
    
    @Test
    public void testShowVetsSpecialties() throws Exception{
    	mockMvc.perform(get("/vets/vetSpecialties")).
    	andExpect(status().isOk()).
    	andExpect(model().attributeExists("specialties")).
    	andExpect(view().name("vets/vetSpecialties"));
    }

    @Test
    public void testShowResourcesVetList() throws Exception {
        ResultActions actions = mockMvc.perform(get("/vets")
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        actions.andExpect(content().contentType("application/json;charset=UTF-8"))
            .andExpect(jsonPath("$.vetList[0].id").value(1));
    }
    
    @Test
    public void testShowVet() throws Exception{
    	mockMvc.perform(get("/vets/{vetId}", TEST_VET_ID))
    	.andExpect(status().isOk())
    	.andExpect(model().attributeExists("vet"))
    	.andExpect(view().name("vets/vetDetails"));
    }

    @Test
    public void testInCreationForm() throws Exception {
    	mockMvc.perform(get("/vets/new")).
    	andExpect(status().isOk()).
    	andExpect(model().attributeExists("vet")).
    	andExpect(view().name("vets/createOrUpdateVetForm"));
    }
    
    @Test
    public void testProcessCreationForm() throws Exception {
    	mockMvc.perform(post("/vets/new").
    			param("firstName", "Javier").
    			param("lastName", "Carmona")).
    	andExpect(status().is3xxRedirection()).
    	andExpect(view().name("redirect:/vets.html"));
    }
    
}

