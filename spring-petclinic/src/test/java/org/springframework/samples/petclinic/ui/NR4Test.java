package org.springframework.samples.petclinic.ui;


import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR4Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
	  driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testNR4() throws Exception {
    driver.get("http://localhost:50000/");
    driver.findElement(By.linkText("FIND OWNERS")).click();
    driver.findElement(By.linkText("Add Owner")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("Javier");
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Carmona");
    driver.findElement(By.id("address")).clear();
    driver.findElement(By.id("address")).sendKeys("Calle Larga 123");
    driver.findElement(By.id("city")).clear();
    driver.findElement(By.id("city")).sendKeys("Cáceres");
    driver.findElement(By.id("telephone")).clear();
    driver.findElement(By.id("telephone")).sendKeys("963852741");
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::button[1]")).click();
    assertEquals("Javier Carmona", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::b[1]")).getText());
    assertEquals("Calle Larga 123", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Address'])[1]/following::td[1]")).getText());
    assertEquals("Cáceres", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/following::td[1]")).getText());
    assertEquals("963852741", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::td[1]")).getText());
    assertEquals("Delete Owner", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::a[2]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Error'])[1]/following::div[2]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Telephone'])[1]/following::a[2]")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
