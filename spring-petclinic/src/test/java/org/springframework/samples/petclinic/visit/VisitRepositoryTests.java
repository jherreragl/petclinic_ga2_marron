/*
 * Copyright 2016-2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.visit;

/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import java.time.LocalDate;
import org.springframework.stereotype.Repository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {
	
	public Visit visit;
	
	@Autowired
	public VisitRepository VR;
	
	public Vet vet;
	
	@Autowired
	public VetRepository vetR;
	
	@Before
	public void init() {
		if(visit==null) {
			visit = new Visit();
			visit.setId(1);
			visit.setDate(LocalDate.now());
			visit.setDescription("Prueba");
			visit.setPetId(1);
			vet=new Vet();
			vet.setFirstName("Prueba");
			vet.setHomeVisits(false);
			vet.setLastName("Prueba");
			vetR.save(vet);
			visit.setVet(vet);
			VR.save(visit);
		}
	}
	
	@Test
	public void testVisit() {
		assertNotNull(visit);				
	}

	
	@Test
	public void testVR()	{
		Visit findById = VR.findById(1);
		assertEquals(visit.getDate(), findById.getDate());
		assertEquals(visit.getDescription(), findById.getDescription());
		assertEquals(visit.getId(), findById.getId());
		assertEquals(visit.getPetId(), findById.getPetId());
		
		assertEquals(visit.getVet().getFirstName(), findById.getVet().getFirstName());
		assertEquals(visit.getVet().getLastName(), findById.getVet().getLastName());
		assertEquals(visit.getVet().getId(), findById.getVet().getId());
		assertEquals(visit.getVet().getHomeVisits(), findById.getVet().getHomeVisits());
	}
	
	@After
	public void doAfter() {
		vet=null;
		visit=null;
	}
	
}