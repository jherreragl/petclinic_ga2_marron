package org.springframework.samples.petclinic.ui;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class NR2Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
		driver = new ChromeDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testShowNameSpeciality() throws Exception {
    driver.get("http://localhost:50000/");
    assertEquals("VETERINARIANS", driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find pets'])[1]/following::span[2]")).getText());
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Find pets'])[1]/following::span[2]")).click();
    driver.findElement(By.linkText("Add Vet")).click();
    driver.findElement(By.id("firstName")).click();
    driver.findElement(By.id("firstName")).clear();
    driver.findElement(By.id("firstName")).sendKeys("PruebaVet");
    driver.findElement(By.id("lastName")).click();
    driver.findElement(By.id("lastName")).clear();
    driver.findElement(By.id("lastName")).sendKeys("Erinario");
    // ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=specialties | label=radiology]]
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::option[2]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::button[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='No'])[4]/following::td[1]")).click();
    String nombreVet = driver.findElement(By.linkText("PruebaVet Erinario")).getText();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='PruebaVet Erinario'])[1]/following::td[1]")).click();
    String especVet = driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='PruebaVet Erinario'])[1]/following::span[1]")).getText();
    driver.findElement(By.linkText("PruebaVet Erinario")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::td[1]")).click();
    driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::td[1]")).click();
    assertEquals(nombreVet, driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::b[1]")).getText());
    assertEquals(especVet, driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Specialties'])[1]/following::span[1]")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
