package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTest {

	@Autowired
	private OwnerRepository owners;
	
	private Owner owner;
	
	
	@Before
	public void init() {
		if(this.owner == null) {
			owner = new Owner();
			owner.setFirstName("Javier");
			owner.setLastName("Carmona");
			owner.setAddress("Calle Larga 123");
			owner.setCity("Caceres");
			owner.setTelephone("963852741");
			
			this.owners.save(owner);
		}
	}
	
	@Test
	public void testDeleteOwnerById() {
		owners.deleteById(owner.getId());
		assertNull(owners.findById(owner.getId()));
	}
	
	@After
	public void finish() {
		this.owner = null;
	}
}
